/**
 * Copyright (c) 2012 Tom Schaible
 * See the file license.txt for copying permission.
 */
package fr.ge.common.support.i18n;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A resource bundle control for loading gettext po files.
 *
 * This expects po files to be stored in a "location" and named with a "domain".
 *
 * A po file with a "locale" of en_US a "location" of locale and a "domain" of
 * lang will be retrievied from:
 *
 * location/en_US/lang.po
 *
 * @author Tom Schaible
 */
public final class GettextResourceBundleControl extends Control {

    /** The resource bundles. */
    private static final Map<String, GettextResourceBundleControl> RESOURCE_BUNDLES = new ConcurrentHashMap<>();

    /** Number of seconds for the cache TTL. */
    private static final int CACHE_TTL_NB_SECONDS = 60;

    /** Milliseconds in a second. */
    private static final long MILLIS_IN_SECOND = 1000L;

    /** Recheck files every 60 seconds. */
    private static long cacheTTL = CACHE_TTL_NB_SECONDS * MILLIS_IN_SECOND;

    /** File load times. */
    private final Map<File, Long> fileLoadTimes;

    /** Domain. */
    private final String domain;

    /**
     * Gets the control.
     *
     * @param domain
     *            the domain
     * @return the control
     */
    public static synchronized GettextResourceBundleControl getControl(final String domain) {
        GettextResourceBundleControl control = RESOURCE_BUNDLES.get(domain);
        if (control == null) {
            control = new GettextResourceBundleControl(domain);
            RESOURCE_BUNDLES.put(domain, control);
        }
        return control;
    }

    /**
     * Sets the cache TTL.
     *
     * @param cacheTTL
     *            the cache TTL
     */
    public static void setCacheTTL(final long cacheTTL) {
        GettextResourceBundleControl.cacheTTL = cacheTTL;
    }

    /**
     * Constructor.
     *
     * @param domain
     *            the domain name to use for po files
     */
    private GettextResourceBundleControl(final String domain) {
        super();
        this.domain = domain;
        this.fileLoadTimes = new ConcurrentHashMap<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getFormats(final String baseName) {
        if (baseName == null) {
            throw new NullPointerException();
        }
        return Arrays.asList("po");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResourceBundle newBundle(final String baseName, final Locale locale, final String format, final ClassLoader loader, //
            final boolean reload) throws IOException {
        if (baseName == null || locale == null || format == null || loader == null) {
            throw new NullPointerException();
        }
        ResourceBundle resourceBundle = null;
        if (format.equals("po")) {
            final String bundleName = this.toPoBundleName(baseName, locale);
            final String resourceName = this.toPoResourceName(bundleName, format);
            final File file = new File(resourceName);
            final URL url = loader.getResource(resourceName);
            resourceBundle = this.getNewBundleFromFile(loader, reload, resourceName, file, url);
        }
        return resourceBundle;
    }

    /**
     * Gets a new bundle from a file.
     *
     * @param loader
     *            the loader
     * @param reload
     *            reload ?
     * @param resourceName
     *            the resource name
     * @param file
     *            the file
     * @param url
     *            the URL
     * @return a new bundle
     * @throws IOException
     *             an {@link IOException}
     */
    private ResourceBundle getNewBundleFromFile(final ClassLoader loader, final boolean reload, final String resourceName, //
            final File file, final URL url) throws IOException {
        ResourceBundle resourceBundle = null;
        if (file.exists()) {
            resourceBundle = new GettextResourceBundle(file);
            this.fileLoadTimes.put(file, file.lastModified());
        } else if (url != null) {
            InputStream stream = null;
            if (reload) {
                final URLConnection connection = url.openConnection();
                if (connection != null) {
                    connection.setUseCaches(false);
                    stream = connection.getInputStream();
                }
            } else {
                stream = loader.getResourceAsStream(resourceName);
            }
            if (stream != null) {
                resourceBundle = new GettextResourceBundle(stream);
            }
        }
        return resourceBundle;
    }

    /**
     * Gets the PO resource name.
     *
     * @param bundleName
     *            the bundle name
     * @param format
     *            the format
     * @return the PO resource name
     */
    String toPoResourceName(final String bundleName, final String format) {
        if (bundleName == null || format == null) {
            throw new NullPointerException();
        }
        return bundleName + "/LC_MESSAGES/" + this.domain + ".po";
    }

    /**
     * Gets the PO bundle name.
     *
     * @param baseName
     *            the base name
     * @param locale
     *            the locale
     * @return the PO bundle name
     */
    String toPoBundleName(final String baseName, final Locale locale) {
        if (baseName == null || locale == null) {
            throw new NullPointerException();
        }
        return baseName + "/" + locale.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTimeToLive(final String baseName, final Locale locale) {
        return GettextResourceBundleControl.cacheTTL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean needsReload(final String baseName, final Locale locale, final String format, final ClassLoader loader, //
            final ResourceBundle bundle, final long loadTime) {
        boolean needsReload = false;

        if (format.equals("po")) {
            final String bundleName = this.toBundleName(baseName, locale);
            final String resourceName = this.toPoResourceName(bundleName, format);

            final File file = new File(resourceName);
            // only attempt to reload files
            if (file.exists()) {
                // reload if no lastModifed time is tracked or it does not match
                // the current
                // last modified time of the file
                if (this.fileLoadTimes == null || this.fileLoadTimes.get(file) != file.lastModified()) {
                    needsReload = true;
                }

            }
        }

        return needsReload;
    }

}
