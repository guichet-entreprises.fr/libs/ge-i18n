/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.support.i18n;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Message reader.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class MessageReader {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageReader.class);

    /** Resource bundle. */
    protected final ResourceBundle resourceBundle;

    /** Message formatter. */
    protected MessageFormatter messageFormatter;

    /**
     * Constructor.
     *
     * @param resourceBundle
     *            the resource bundle
     */
    protected MessageReader(final ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

    /**
     * Gets the bundle.
     *
     * @param baseName
     *            the base name
     * @param locale
     *            the locale
     * @return the bundle
     */
    private static ResourceBundle getBundle(final String baseName, final Locale locale) {
        ResourceBundle gotBundle = null;
        final String bundleBaseName = baseName == null ? "po/ui" : baseName;
        final Locale bundleLocale = locale == null ? LocaleContextHolder.getLocale() : locale;
        if (bundleLocale == null) {
            LOGGER.debug("No bundle found for baseName='{}', locale='null'", bundleBaseName);
        } else {
            try {
                gotBundle = ResourceBundle.getBundle(bundleBaseName, bundleLocale, GettextResourceBundleControl.getControl("messages"));
            } catch (MissingResourceException | IllegalArgumentException e) {
                LOGGER.info("No bundle found for baseName='{}', locale='{}'", bundleBaseName, bundleLocale);
                LOGGER.trace(StringUtils.EMPTY, e);
            }
        }
        return gotBundle;
    }

    /**
     * Gets the message reader from po/ui/[lang]/LC_MESSAGES/messages.po in the
     * resources.
     *
     * @return the message reader
     */
    public static MessageReader getReader() {
        return getReader(MessageReader.getBundle(null, null));
    }

    /**
     * Gets the message reader from po/ui/[lang]/LC_MESSAGES/messages.po in the
     * resources.
     *
     * @param locale
     *            the locale
     * @return the message reader
     */
    public static MessageReader getReader(final Locale locale) {
        return getReader(MessageReader.getBundle(null, locale));
    }

    /**
     * Gets the message reader from [baseName]/[lang]/LC_MESSAGES/messages.po.
     *
     * @param baseName
     *            the base name
     * @return the message reader
     */
    public static MessageReader getReader(final String baseName) {
        return getReader(MessageReader.getBundle(baseName, null));
    }

    /**
     * Gets the message reader from [baseName]/[lang]/LC_MESSAGES/messages.po.
     *
     * @param baseName
     *            the base name
     * @param locale
     *            the locale
     * @return the message reader
     */
    public static MessageReader getReader(final String baseName, final Locale locale) {
        return getReader(MessageReader.getBundle(baseName, locale));
    }

    /**
     * Gets the message reader from a resource bundle.
     *
     * @param bundle
     *            resource bundle
     * @return message reader
     */
    public static MessageReader getReader(final ResourceBundle bundle) {
        return new MessageReader(bundle);
    }

    /**
     * Gets the bundle.
     *
     * @return the bundle
     */
    public ResourceBundle getBundle() {
        return this.resourceBundle;
    }

    /**
     * Gets the message formatter.
     *
     * @return the message formatter
     */
    public MessageFormatter getFormatter() {
        if (null == this.messageFormatter) {
            if (null == this.resourceBundle) {
                this.messageFormatter = new EmptyMessageFormatter();
            } else {
                this.messageFormatter = new BundleMessageFormatter(this.resourceBundle);
            }
        }
        return this.messageFormatter;
    }

}
