/**
 * Copyright (c) 2012 Tom Schaible
 * See the file license.txt for copying permission.
 */
package fr.ge.common.support.i18n;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A resource bundle that is created from a gettext PO file.
 * https://github.com/tschaible/gettext-resource-bundle.
 *
 * @author Tom Schaible
 */
public class GettextResourceBundle extends ResourceBundle {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(GettextResourceBundle.class);

    /** Line pattern. */
    private static final Pattern LINE_PATTERN = Pattern.compile("^([\\w_\\[\\]]*)\\s*\\\"(.*)\\\"$");

    /** Resources. */
    private final Map<String, Object> resources = new HashMap<>();

    /**
     * Constructor.
     *
     * @param inputStream
     *            the input stream
     */
    public GettextResourceBundle(final InputStream inputStream) {
        this.init(new LineNumberReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)));
    }

    /**
     * Constructor.
     *
     * @param reader
     *            the reader
     */
    public GettextResourceBundle(final Reader reader) {
        this.init(new LineNumberReader(reader));
    }

    /**
     * Constructor.
     *
     * @param file
     *            the file
     */
    public GettextResourceBundle(final File file) {
        try (Reader fileReader = new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8")); LineNumberReader in = new LineNumberReader(fileReader)) {
            this.init(in);
        } catch (final IOException e) {
            LOGGER.error("GettextResourceBundle could not be initialized", e);
        }
    }

    /**
     * Initializes the ResourceBundle from a PO file.
     *
     * @param reader
     *            the reader to read the contents of the PO file from
     */
    private void init(final LineNumberReader reader) {
        if (reader == null) {
            LOGGER.warn("GettextResourceBundle could not be initialized, input was null");
        } else {
            String line = null;
            final Map<String, String> keyValue = new HashMap<>();
            try {
                while ((line = reader.readLine()) != null) {
                    if (line.startsWith("#")) {
                        LOGGER.trace(reader.getLineNumber() + ": Parsing PO file, comment skipped [" + line + "]");
                    } else if (line.trim().length() == 0) {
                        LOGGER.trace(reader.getLineNumber() + ": Parsing PO file, whitespace line skipped");
                    } else {
                        this.readLine(reader, line, keyValue);
                    }
                }
                final String key = keyValue.get("key");
                final String value = keyValue.get("value");
                if (key != null && value != null) {
                    LOGGER.trace("Parsing PO file, key,value pair found [" + key + " => " + value + "]");
                    this.resources.put(StringEscapeUtils.unescapeJava(key), StringEscapeUtils.unescapeJava(value));
                }
            } catch (final IOException e) {
                LOGGER.error("GettextResourceBundle could not be initialized", e);
            }
        }
        LOGGER.info("GettextResourceBundle initialization complete, " + this.resources.size() + " resources loaded");

    }

    /**
     * Reads a line.
     *
     * @param reader
     *            the reader
     * @param line
     *            the line
     * @param keyValue
     *            the key-value map
     */
    private void readLine(final LineNumberReader reader, final String line, final Map<String, String> keyValue) {
        String key = keyValue.get("key");
        String value = keyValue.get("value");
        final Matcher matcher = LINE_PATTERN.matcher(line);
        if (matcher.matches()) {
            final String type = matcher.group(1);
            final String str = matcher.group(2);
            if ("msgid".equals(type)) {
                if (key != null && value != null) {
                    LOGGER.trace("Parsing PO file, key,value pair found [" + key + " => " + value + "]");
                    this.resources.put(StringEscapeUtils.unescapeJava(key), StringEscapeUtils.unescapeJava(value));
                    key = null;
                    value = null;
                }
                key = str;
                LOGGER.trace(reader.getLineNumber() + ": Parsing PO file, msgid found [" + key + "]");
            } else if ("msgstr".equals(type)) {
                value = str;
                LOGGER.trace(reader.getLineNumber() + ": Parsing PO file, msgstr found [" + value + "]");
            } else if (type == null || type.length() == 0) {
                if (value == null) {
                    LOGGER.trace(reader.getLineNumber() + ": Parsing PO file, addition to msgid found [" + str + "]");
                    key += str;
                } else {
                    LOGGER.trace(reader.getLineNumber() + ": Parsing PO file, addition to msgstr found [" + str + "]");
                    value += str;
                }
            }
            keyValue.put("key", key);
            keyValue.put("value", value);
        } else {
            LOGGER.error(reader.getLineNumber() + ": Parsing PO file, invalid syntax [" + line + "]");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Enumeration<String> getKeys() {
        return Collections.enumeration(this.resources.keySet());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object handleGetObject(final String key) {
        return this.resources.get(key);
    }

}
