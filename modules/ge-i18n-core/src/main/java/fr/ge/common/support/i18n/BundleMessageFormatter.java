/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.support.i18n;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link MessageFormatter} based on a {@link ResourceBundle}.
 *
 * @author Christian Cougourdan
 */
public class BundleMessageFormatter implements MessageFormatter {

    private static final Logger LOGGER = LoggerFactory.getLogger(BundleMessageFormatter.class);

    /** Resource bundle. */
    private final ResourceBundle resourceBundle;

    /**
     * Constructor.
     *
     * @param resourceBundle
     *            the resource bundle
     */
    BundleMessageFormatter(final ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

    /**
     * Formats the message from the key and return the key if no message can be
     * found.
     *
     * @param key
     *            the key
     * @param params
     *            the params
     * @return the message
     */
    @Override
    public String format(final String key, final Object... params) {
        return this.format(key, false, params);
    }

    /**
     * Formats the message from the key and return null if no message can be found.
     *
     * @param key
     *            the key
     * @param params
     *            the params
     * @return the message
     */
    @Override
    public String formatNullIfMissing(final String key, final Object... params) {
        return this.format(key, true, params);
    }

    /**
     * Formats the message from the key and handles resource bundles problems.
     *
     * @param key
     *            the key
     * @param nullIfMissing
     *            null if missing ?
     * @param params
     *            the params
     * @return the message
     */
    private String format(final String key, final boolean nullIfMissing, final Object... params) {
        if (this.resourceBundle == null) {
            LOGGER.debug("No message found for bundle='null', key='{}'", key);
            return nullIfMissing ? null : key;
        }
        String msg = nullIfMissing ? null : StringUtils.EMPTY;
        if (key == null) {
            LOGGER.debug("No message found for baseName='{}', locale='{}', key='null'", //
                    this.resourceBundle.getBaseBundleName(), this.resourceBundle.getLocale());
        } else {
            msg = this.getMessageFromKey(key, nullIfMissing, msg, params);
        }
        if (this.resourceBundle.getLocale() == null) {
            LOGGER.trace("Resolved code '{}' with memory bundle into '{}'", key, msg);
        } else {
            LOGGER.trace("Resolved code '{}' with locale '{}' into '{}'", key, this.resourceBundle.getLocale(), msg);
        }
        return msg;
    }

    /**
     * Gets the message from the key.
     *
     * @param key
     *            the key
     * @param nullIfMissing
     *            null if missing ?
     * @param msg
     *            the message
     * @param params
     *            the params
     * @return the message
     */
    private String getMessageFromKey(final String key, final boolean nullIfMissing, final String msg, final Object... params) {
        String formattedMsg = msg;
        try {
            formattedMsg = this.resourceBundle.getString(key);
        } catch (final MissingResourceException | ClassCastException e) {
            if (!nullIfMissing) {
                formattedMsg = key;
            }
            LOGGER.debug(e.getMessage());
            if (this.resourceBundle.getLocale() == null) {
                LOGGER.debug("No message found for memory bundle, key='{}'", key);
            } else {
                LOGGER.debug("No message found for baseName='{}', locale='{}', key='{}'", //
                        this.resourceBundle.getBaseBundleName(), this.resourceBundle.getLocale(), key);
            }
            if (LOGGER.isTraceEnabled()) {
                if (this.resourceBundle.getLocale() == null) {
                    LOGGER.trace("Available keys for memory bundle are : {}", this.resourceBundle.keySet());
                } else {
                    LOGGER.trace("Available keys for baseName='{}', locale='{}' are : {}", this.resourceBundle.getBaseBundleName(), //
                            this.resourceBundle.getLocale(), this.resourceBundle.keySet());
                }
                LOGGER.trace(StringUtils.EMPTY, e);
            }
        }
        if (formattedMsg != null && ArrayUtils.isNotEmpty(params)) {
            formattedMsg = formattedMsg.replaceAll("'", "''");
            formattedMsg = MessageFormat.format(formattedMsg, params);
        }
        return formattedMsg;
    }

}
