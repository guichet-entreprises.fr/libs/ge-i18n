/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.support.i18n;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests {@link MessageReader}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class MessageReaderTest {

    /**
     * Setup.
     */
    @Before
    public void setup() {
        System.out.println("###### testFormat ###### Locale : " + Locale.getDefault());
        System.out.println("###### testFormat ###### TimeZone : " + TimeZone.getDefault().getID());
    }

    /**
     * Tests {@link MessageReader#getReader(Locale)}.
     */
    @Test
    public void testDefaultFormat() {
        MessageFormatter formatter = MessageReader.getReader(Locale.ENGLISH).getFormatter();
        assertEquals("Random UI label", formatter.format("Random UI label"));
        assertEquals("Random UI label", formatter.formatNullIfMissing("Random UI label"));

        formatter = MessageReader.getReader(Locale.FRENCH).getFormatter();
        assertEquals("Libellé d'IHM quelconque", formatter.format("Random UI label"));
        assertEquals("Libellé d'IHM quelconque", formatter.formatNullIfMissing("Random UI label"));

        formatter = MessageReader.getReader(new Locale("es", "ES")).getFormatter();
        assertEquals("Cualquier interfaz texto", formatter.format("Random UI label"));
        assertEquals("Cualquier interfaz texto", formatter.formatNullIfMissing("Random UI label"));
    }

    /**
     * Tests {@link MessageReader#getReader(String, Locale)}.
     */
    @Test
    public void testDefaultFormatParameters() {
        final MessageFormatter formatter = MessageReader.getReader(Locale.FRENCH).getFormatter();
        assertEquals("arg1 Libellé d'IHM quelconque arg2", formatter.format("Random UI label {0} {1}", "arg1", "arg2"));
        assertEquals("arg1 Libellé d'IHM quelconque arg2", formatter.formatNullIfMissing("Random UI label {0} {1}", "arg1", "arg2"));
    }

    /**
     * Tests {@link MessageReader#getReader(Locale)}.
     */
    @Test
    public void testDefaultFormatNoBundle() {
        final MessageFormatter formatter = MessageReader.getReader(Locale.ITALIAN).getFormatter();
        assertEquals("Libellé d'IHM quelconque", formatter.format("Random UI label"));
        assertEquals("Libellé d'IHM quelconque", formatter.formatNullIfMissing("Random UI label"));
    }

    /**
     * Tests {@link MessageReader#getReader(Locale)}.
     */
    @Test
    public void testDefaultFormatNoMessage() {
        final MessageFormatter formatter = MessageReader.getReader(Locale.FRENCH).getFormatter();
        assertEquals("message that does not exist", formatter.format("message that does not exist"));
        assertEquals(null, formatter.formatNullIfMissing("message that does not exist"));
    }

    /**
     * Tests {@link MessageReader#getReader(Locale)}.
     */
    @Test
    public void testDefaultFormatNullMessage() {
        final MessageFormatter formatter = MessageReader.getReader(Locale.FRENCH).getFormatter();
        assertEquals("", formatter.format(null));
        assertEquals(null, formatter.formatNullIfMissing(null));
    }

    /**
     * Tests {@link MessageReader#getReader(String, Locale)}.
     */
    @Test
    public void testBaseNameFormat() {
        MessageFormatter formatter = MessageReader.getReader("po/tests", Locale.ENGLISH).getFormatter();
        assertEquals("HelloWorld", formatter.format("HelloWorld"));
        assertEquals("HelloWorld", formatter.formatNullIfMissing("HelloWorld"));

        formatter = MessageReader.getReader("po/tests", Locale.FRENCH).getFormatter();
        assertEquals("BonjourMonde", formatter.format("HelloWorld"));
        assertEquals("BonjourMonde", formatter.formatNullIfMissing("HelloWorld"));

        formatter = MessageReader.getReader("po/tests", new Locale("es", "ES")).getFormatter();
        assertEquals("OlaMundo", formatter.format("HelloWorld"));
        assertEquals("OlaMundo", formatter.formatNullIfMissing("HelloWorld"));
    }

    /**
     * Tests {@link MessageReader#getReader(String, Locale)}.
     */
    @Test
    public void testBaseNameFormatNullLocale() {
        final MessageFormatter formatter = MessageReader.getReader("po/tests", null).getFormatter();
        assertEquals("BonjourMonde", formatter.format("HelloWorld"));
        assertEquals("BonjourMonde", formatter.formatNullIfMissing("HelloWorld"));
    }

    /**
     * Tests {@link MessageReader#getReader(String, Locale)}.
     */
    @Test
    public void testBaseNameFormatParameters() {
        final MessageFormatter formatter = MessageReader.getReader("po/tests", Locale.FRENCH).getFormatter();
        assertEquals("arg1 BonjourMonde arg2", formatter.format("HelloWorld {0} {1}", "arg1", "arg2"));
        assertEquals("arg1 BonjourMonde arg2", formatter.formatNullIfMissing("HelloWorld {0} {1}", "arg1", "arg2"));
    }

    /**
     * Tests {@link MessageReader#getReader(String, Locale)}.
     */
    @Test
    public void testBaseNameFormatNoBundle() {
        final MessageFormatter formatter = MessageReader.getReader("po/tests", Locale.ITALIAN).getFormatter();
        assertEquals("BonjourMonde", formatter.format("HelloWorld"));
        assertEquals("BonjourMonde", formatter.formatNullIfMissing("HelloWorld"));
    }

    /**
     * Tests {@link MessageReader#getReader(String, Locale)}.
     */
    @Test
    public void testBaseNameFormatNoMessage() {
        final MessageFormatter formatter = MessageReader.getReader("po/tests", Locale.FRENCH).getFormatter();
        assertEquals("message that does not exist", formatter.format("message that does not exist"));
        assertEquals(null, formatter.formatNullIfMissing("message that does not exist"));
    }

    /**
     * Tests {@link MessageReader#getReader(String, Locale)}.
     */
    @Test
    public void testBaseNameFormatNullMessage() {
        final MessageFormatter formatter = MessageReader.getReader("po/tests", Locale.FRENCH).getFormatter();
        assertEquals("", formatter.format(null));
        assertEquals(null, formatter.formatNullIfMissing(null));
    }

    @Test
    public void testNoBundle() throws Exception {
        final MessageReader reader = MessageReader.getReader((ResourceBundle) null);
        final MessageFormatter formatter = reader.getFormatter();

        assertTrue(formatter instanceof EmptyMessageFormatter);
        assertEquals(null, formatter.formatNullIfMissing("Here is a {} test", "formatter"));
        assertEquals("Here is a {0} test", formatter.format("Here is a {0} test"));
        assertEquals("Here is a formatter test", formatter.format("Here is a {0} test", "formatter"));
        assertEquals("Simple message", formatter.format("Simple message"));
        assertEquals("Simple message", formatter.format("Simple message", "formatter"));
    }

}
