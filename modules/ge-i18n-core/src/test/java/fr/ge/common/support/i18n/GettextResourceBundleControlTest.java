/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.support.i18n;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Test;

/**
 * The Class GettextResourceBundleControlTest.
 *
 * @author Tom Schaible
 * 
 *         Unit tests for GettextResourceBundleControl
 */
public class GettextResourceBundleControlTest {

    /**
     * test that the controls picks up PO files by locale from the classpath.
     */
    @Test
    public void testClasspathReads() {
        final ResourceBundle fr = ResourceBundle.getBundle("po/ui", Locale.FRENCH, GettextResourceBundleControl.getControl("messages"));
        assertEquals("Libellé d'IHM quelconque", fr.getString("Random UI label"));

        final ResourceBundle en = ResourceBundle.getBundle("po/ui", Locale.ENGLISH, GettextResourceBundleControl.getControl("messages"));
        assertEquals("Random UI label", en.getString("Random UI label"));
    }

    /**
     * test that the controls picks up PO files by locale from the filesystem.
     */
    @Test
    public void testFileReads() {
        final ResourceBundle fr = ResourceBundle.getBundle("./src/test/resources/po/ui", Locale.FRENCH, GettextResourceBundleControl.getControl("messages"));
        assertEquals("Libellé d'IHM quelconque", fr.getString("Random UI label"));

        final ResourceBundle en = ResourceBundle.getBundle("./src/test/resources/po/ui", Locale.ENGLISH, GettextResourceBundleControl.getControl("messages"));
        assertEquals("Random UI label", en.getString("Random UI label"));
    }

    /**
     * test that PO files loaded from the file system will reload after cache
     * timeout occurs and modified dates are different.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Ignore("Copies files and needs to wait for cache TTL")
    @Test
    public void testFileReloads() throws IOException {
        final File poOldDirectory = new File("./src/test/resources/po/ui");
        final File poNewDirectory = new File("./src/test/resources/po/ui2");
        final File targetDirectory = new File("./target/testLocales");
        targetDirectory.mkdirs();

        // unit test with 2 second cache time
        final long cacheTTL = 5 * 1000l;
        GettextResourceBundleControl.setCacheTTL(cacheTTL);

        FileUtils.copyDirectory(poOldDirectory, targetDirectory, false);

        ResourceBundle fr = ResourceBundle.getBundle(targetDirectory.getAbsolutePath(), Locale.FRENCH, GettextResourceBundleControl.getControl("messages"));
        assertEquals("Libellé d'IHM quelconque", fr.getString("Random UI label"));

        ResourceBundle en = ResourceBundle.getBundle(targetDirectory.getAbsolutePath(), Locale.ENGLISH, GettextResourceBundleControl.getControl("messages"));
        assertEquals("Random UI label", en.getString("Random UI label"));

        FileUtils.copyDirectory(poNewDirectory, targetDirectory, false);

        fr = ResourceBundle.getBundle(targetDirectory.getAbsolutePath(), Locale.FRENCH, GettextResourceBundleControl.getControl("messages"));
        assertEquals("Libellé d'IHM quelconque", fr.getString("Random UI label"));

        en = ResourceBundle.getBundle(targetDirectory.getAbsolutePath(), Locale.ENGLISH, GettextResourceBundleControl.getControl("messages"));
        assertEquals("Random UI label", en.getString("Random UI label"));

        // wait until cacheTTL passes so that cache expires
        final long end = System.currentTimeMillis() + cacheTTL;
        while (end > System.currentTimeMillis()) {
            try {
                Thread.sleep(end - System.currentTimeMillis());
            } catch (final InterruptedException e) {

            }
        }

        fr = ResourceBundle.getBundle(targetDirectory.getAbsolutePath(), Locale.FRENCH, GettextResourceBundleControl.getControl("messages"));
        assertEquals("Libellé d'IHM quelconque 2", fr.getString("Random UI label"));

        en = ResourceBundle.getBundle(targetDirectory.getAbsolutePath(), Locale.ENGLISH, GettextResourceBundleControl.getControl("messages"));
        assertEquals("Random UI label 2", en.getString("Random UI label"));
    }

}
