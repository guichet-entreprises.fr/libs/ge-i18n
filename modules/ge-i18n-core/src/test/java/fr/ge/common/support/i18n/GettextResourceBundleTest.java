/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.support.i18n;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.text.MessageFormat;

import org.junit.Test;

/**
 * The Class GettextResourceBundleTest.
 *
 * @author Tom Schaible
 * 
 *         Unit tests for GettextResourceBundle
 */
public class GettextResourceBundleTest {

    /**
     * test a PO file containing normal text specifically test - a normal key -
     * keys spread across multiple values - keys containing the newline string.
     */
    @Test
    public void testNormalPOFile() {
        final GettextResourceBundle rb = new GettextResourceBundle(Thread.currentThread().getContextClassLoader().getResourceAsStream("po/parser/test.po"));

        assertEquals("Testing fetch of normal key/value pair", rb.getString("key1"), "value1");
        assertEquals("Testing fetch of key/values extended over multiple lines", rb.getString("key2"), "value2");
        assertTrue("Testing containsKey returns true for present keys", rb.containsKey("key3\nwith some other stuff"));
        assertFalse("Testing containsKey returns false for missing keys", rb.containsKey("key3\\nwith some other stuff"));
        assertEquals("Testing fetch of key/values extended over multiple lines", rb.getString("key3\nwith some other stuff"), "value3\nwith some different stuff");
    }

    /**
     * test to ensure special characters and escaping are parsed correctly.
     */
    @Test
    public void testSpecialCharactersPOFile() {
        final GettextResourceBundle rb = new GettextResourceBundle(Thread.currentThread().getContextClassLoader().getResourceAsStream("po/parser/special-chars.po"));

        assertEquals("Testing escaped characters are properly parsed", rb.getString("allspecialchars"), "\t\b\n\r\f\'\"\\\t");
        assertEquals("Testing escaping of escaped characters are properly parsed", rb.getString("escapedspecialchars"), "\\t\\b\\n\\r\\f\\'\\\"");
        assertEquals("Testing escaped unicode characters are properly parsed", rb.getString("escapedunicode"), "π");
        assertEquals("Testing escaped octal characters are properly parsed", rb.getString("escapedoctal"), "aBc^");
    }

    /**
     * test that messages for MessageFormatter and Formatter can be parsed and
     * used correctly.
     */
    @Test
    public void testReplacementsPOFile() {
        final GettextResourceBundle rb = new GettextResourceBundle(Thread.currentThread().getContextClassLoader().getResourceAsStream("po/parser/replacements.po"));
        assertEquals("Testing MessageFormat", MessageFormat.format(rb.getString("MessageFormat"), "replace", Math.PI), "successful replacement [replace] [3,14]");
        assertEquals("Testing Formatter", String.format(rb.getString("Formatter"), "replace", Math.PI), "successful replacement [replace] [3,14]");
    }

    /**
     * test that UTF-8 message are correctly read with french accent.
     */
    @Test
    public void testUnixUTF8POFile() {
        final GettextResourceBundle rb = new GettextResourceBundle(Thread.currentThread().getContextClassLoader().getResourceAsStream("po/encoding/french_utf8.po"));
        assertEquals("Testing French accent é", rb.getString("I was born on the 1st of february"), "Je suis né le 1er février");
    }

    /**
     * test that ANSI (ISO-8859-1) po file are not correctly read with french
     * accent.
     */
    @Test
    public void testUnixAnsiPOFile() {
        final GettextResourceBundle rb = new GettextResourceBundle(Thread.currentThread().getContextClassLoader().getResourceAsStream("po/encoding/french_ansi.po"));
        assertNotEquals("Testing French accent é", rb.getString("I was born on the 1st of february"), "Je suis né le 1er février");
    }

}
