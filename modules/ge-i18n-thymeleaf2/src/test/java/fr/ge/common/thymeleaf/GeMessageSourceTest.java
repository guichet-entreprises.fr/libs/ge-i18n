/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.thymeleaf;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.io.StringWriter;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Tests {@link GeMessageResolver}.
 *
 * @author jpauchet
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/ge-i18n-integration.xml" })
@WebAppConfiguration
public class GeMessageSourceTest {

    /** Template engine. */
    @Autowired
    TemplateEngine engine;

    /**
     * Tests
     * {@link GeMessageResolver#resolveMessage(org.thymeleaf.Arguments, String, Object[])}.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResolve() throws Exception {
        final StringWriter writer = new StringWriter();
        final Context context = new Context(Locale.FRENCH);
        this.engine.process("fr/ge/common/thymeleaf/GeMessageResolverTestResources/resolve", context, writer);
        final String expected = "<!DOCTYPE html>\n\n<html><body><div>Intégration avec Thymeleaf</div></body></html>";
        assertThat(writer.toString(), equalTo(expected));
    }

    /**
     * Tests
     * {@link GeMessageResolver#resolveMessage(org.thymeleaf.Arguments, String, Object[])}.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testResolveArguments() throws Exception {
        final StringWriter writer = new StringWriter();
        final Context context = new Context(Locale.FRENCH);
        this.engine.process("fr/ge/common/thymeleaf/GeMessageResolverTestResources/resolve-arguments", context, writer);
        final String expected = "<!DOCTYPE html>\n\n<html><body><div>Intégration avec Thymeleaf 2 pour la langue fr</div></body></html>";
        assertThat(writer.toString(), equalTo(expected));
    }

}
