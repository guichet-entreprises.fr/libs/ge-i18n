/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.thymeleaf;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.thymeleaf.testing.templateengine.engine.AbstractThymeleafTest;

/**
 * Tests {@link GeMessageSource}.
 *
 * @author jpauchet
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/ge-i18n-integration.xml" })
@WebAppConfiguration
public class GeMessageSourceTest extends AbstractThymeleafTest {

    /**
     * Tests
     * {@link GeMessageSource#getMessage(String, Object[], String, Locale)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testResolve() throws Exception {
        this.test("resolve.thtest");
    }

    /**
     * Tests
     * {@link GeMessageSource#getMessage(String, Object[], String, Locale)}.
     *
     * @throws Exception
     *             an {@link Exception}
     */
    @Test
    public void testResolveArguments() throws Exception {
        this.test("resolve-arguments.thtest");
    }

    /**
     * Tests
     * {@link GeMessageSource#getMessage(String, Object[], String, Locale)}.
     */
    @Test
    public void testMessageSourceResolvable() {
        final MessageSourceResolvable resolvable = new DefaultMessageSourceResolvable(new String[] { "Thymeleaf {0} integration (nice !) for {1} language" }, new String[] { "3", "fr" });
        final String message = new GeMessageSource().getMessage(resolvable, Locale.FRENCH);
        Assert.assertEquals("Intégration avec Thymeleaf 3 (super !) pour la langue fr", message);
    }

}
