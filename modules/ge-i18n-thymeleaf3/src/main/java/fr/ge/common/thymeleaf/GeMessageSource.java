/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.thymeleaf;

import java.util.Locale;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.thymeleaf.TemplateEngine;

import fr.ge.common.support.i18n.MessageReader;

/**
 * Message source based on {@link MessageSource}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
public class GeMessageSource implements MessageSource {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(GeMessageSource.class);

    /** Message key template locale. */
    private static final String MSG_KEY_TEMPLATE_LOCALE = "message with key \"{}\" for locale \"{}\".";

    /** Could not resolve message. */
    private static final String COULD_NOT_RESOLVE_MSG = "[THYMELEAF][{}] Could not resolve " + MSG_KEY_TEMPLATE_LOCALE;

    /** Resolving message. */
    private static final String RESOLVING_MSG = "[THYMELEAF][{}] Resolving " + MSG_KEY_TEMPLATE_LOCALE;

    /** Resolved message. */
    private static final String RESOLVED_MSG = "[THYMELEAF][{}] Resolved " + MSG_KEY_TEMPLATE_LOCALE + " with the ";

    /** Resolved message with UI. */
    private static final String RESOLVED_MSG_UI = RESOLVED_MSG + "UI messages.";

    /**
     * Constructor.
     */
    public GeMessageSource() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMessage(final String code, final Object[] args, final String defaultMessage, final Locale locale) {
        Validate.notNull(code, "Message key cannot be null");
        Validate.notNull(args, "Arguments cannot be null");
        Validate.notNull(locale, "Locale in context cannot be null");

        final Object[] logArgs = new Object[] { TemplateEngine.threadIndex(), code, locale };
        LOGGER.trace(RESOLVING_MSG, logArgs);

        // Search in the message source translations
        final String resolvedMessage = MessageReader.getReader().getFormatter().format(code, args);

        // Return the resolved message
        if (resolvedMessage == null) {
            LOGGER.trace(COULD_NOT_RESOLVE_MSG, logArgs);
        } else {
            LOGGER.trace(RESOLVED_MSG_UI, logArgs);
        }
        return resolvedMessage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMessage(final String code, final Object[] args, final Locale locale) throws NoSuchMessageException {
        return this.getMessage(code, args, code, locale);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMessage(final MessageSourceResolvable resolvable, final Locale locale) throws NoSuchMessageException {
        return this.getMessage(resolvable.getCodes()[0], resolvable.getArguments(), resolvable.getDefaultMessage(), locale);
    }

}
